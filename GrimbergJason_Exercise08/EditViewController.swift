//
//  EditViewController.swift
//  GrimbergJason_Exercise08
//
//  Created by Jason Grimberg on 6/16/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {

    @IBOutlet weak var newShipName: UITextField!
    @IBOutlet weak var newShipHP: UITextField!
    @IBOutlet weak var newShipT2: UISwitch!
    
    // Set the new object
    var editNewShip: EveShips?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Set all edit fields to the newest data
        if let editNewShip = editNewShip {
            newShipName.text = editNewShip.shipName
            newShipHP.text = String(editNewShip.shipHP)
            newShipT2.isOn = editNewShip.isT2
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Save any changes that were made to the new ship
    @IBAction func saveChanges() {
        // Make sure that there are no empty fields
        if (newShipName.text?.isEmpty)! || (newShipHP.text?.isEmpty)! {
            displayError()
        } else {
            // Set the new data into the new class
            if let i = editNewShip, let shipName = newShipName.text, let shipHp = Int(newShipHP.text!), let isT2 = Bool?(newShipT2.isOn) {
                i.shipName = shipName
                i.shipHP = shipHp
                i.isT2 = isT2
                // Dismiss the edit controller
                dismiss(animated: true, completion: {print("Ship updated.")})
            } else {
                // Catch any errors
                displayError()
            }
        }
    }

    // Cancel the edit controller without sending any data back
    @IBAction func dismissView() {
        dismiss(animated: true, completion: nil)
    }
    
    // Functin to display any errors that would occure
    func displayError() {
        let alert = UIAlertController(title: "Invalid Input", message: "Please ensure there are no blanks, and all number fields have whole numbers in them!", preferredStyle: .alert)
        let okButton = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(okButton)
        present(alert, animated: true, completion: nil)
    }
}
