//
//  File.swift
//  GrimbergJason_Exercise08
//
//  Created by Jason Grimberg on 6/16/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation

class EveShips {
    
    // Stored properties
    var shipName: String
    var shipHP: Int
    var isT2: Bool
    
    // Computed properties
    var fullDescription: String {
        get {
            return "The ship's name: \(shipName) HP: \(shipHP) Is this a T2 ship?: \(isT2)"
        }
    }
    
    var getHP: String {
        get {
            if isT2 == true {
                let t2ShipHP: Int = shipHP * 2
                return "Since this is a T2 ship your total HP is \(t2ShipHP)"
            } else {
                return "Your ship has a total of \(shipHP) HP"
            }
        }
    }
    
    // Instance methods
    func addHp() {
        shipHP += Int(arc4random_uniform(100) + 1)
    }
    
    func upgrade() {
        if isT2 {
            isT2 = false
        } else {
            isT2 = true
        }
    }
    
    init() {
        shipName = "Default"
        shipHP = 100
        isT2 = false
    }
    
    init(shipName: String, shipHp: Int, isT2: Bool) {
        self.shipName = shipName
        self.shipHP = shipHp
        self.isT2 = isT2
    }
}
