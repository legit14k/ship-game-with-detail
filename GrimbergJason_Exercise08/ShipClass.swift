//
//  ShipClass.swift
//  GrimbergJason_Exercise08
//
//  Created by Jason Grimberg on 6/16/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import Foundation

class EveShips {
    
    // Stored properties
    var shipName: String
    var shipHP: Int
    var isT2: Bool
    
    // Computed properties
    // Look at a detailed view of the current ship
    var fullDescription: String {
        get {
            return "The ship's name: \(shipName)" + "\r\n" + "HP: \(shipHP)" + "\r\n" + "Is this a T2 ship?: \(isT2)"
        }
    }
    
    // Grab a detailed view of the current HP and bonuses
    var getHP: String {
        get {
            if isT2 == true {
                let t2ShipHP: Int = shipHP + (shipHP * 2)
                return "Since this is a T2 ship your bonus HP is +\(t2ShipHP)"
            } else {
                return "Your ship has a total of \(shipHP) HP"
            }
        }
    }
    
    // Instance methods
    func addHp() {
        shipHP += Int(arc4random_uniform(100) + 1)
    }
    
    // Upgrade the current ship to a tier 2
    func upgrade() {
        if isT2 {
            isT2 = false
        } else {
            isT2 = true
        }
    }
    
    init() {
        shipName = "Default"
        shipHP = 100
        isT2 = false
    }
    
    init(shipName: String, shipHp: Int, isT2: Bool) {
        self.shipName = shipName
        self.shipHP = shipHp
        self.isT2 = isT2
    }
}
