//
//  ViewController.swift
//  GrimbergJason_Exercise08
//
//  Created by Jason Grimberg on 6/16/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var shipName: UILabel!
    @IBOutlet weak var shipHP: UILabel!
    @IBOutlet weak var isT2: UILabel!
    @IBOutlet weak var shipDescription: UILabel!
    @IBOutlet weak var fullHPDescription: UILabel!
    
    
    // Call to the class
    var newShip = EveShips()
    
    override func viewDidLoad() {
        // Display all labels
        displayLabels()
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    // Update the labels when the controller appears
    override func viewWillAppear(_ animated: Bool) {
        displayLabels()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // Display/update all labels
    func displayLabels() {
        shipName.text = newShip.shipName
        shipHP.text = String(newShip.shipHP)
        isT2.text = String(newShip.isT2)
        shipDescription.text = newShip.fullDescription
        fullHPDescription.text = newShip.getHP
    }
    // Upgrade the current ship
    @IBAction func btnUpgrade(_ sender: UIButton) {
        newShip.upgrade()
        displayLabels()
    }
    
    // Add hp to the current ship
    @IBAction func addHp(_ sender: UIButton) {
        newShip.addHp()
        displayLabels()
    }
    
    // Send what data we have to the second view controller
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let shipObjectEditor = segue.destination as? EditViewController {
            shipObjectEditor.editNewShip = newShip
        }
    }
}

