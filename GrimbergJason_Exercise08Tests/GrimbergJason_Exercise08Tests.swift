//
//  GrimbergJason_Exercise08Tests.swift
//  GrimbergJason_Exercise08Tests
//
//  Created by Jason Grimberg on 6/16/18.
//  Copyright © 2018 Jason Grimberg. All rights reserved.
//

import XCTest
@testable import GrimbergJason_Exercise08

class GrimbergJason_Exercise08Tests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
